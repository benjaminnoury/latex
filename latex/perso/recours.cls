\ProvidesClass{recours}
\NeedsTeXFormat{LaTeX2e}

\DeclareOption*{%
      \PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[11pt,french]{article}

% \RequirePackage{Cours}

\RequirePackage{fontspec} %OK
\RequirePackage{xunicode} %OK

% pour gérer l'insertion de figures
\RequirePackage{graphicx} %OK
\DeclareGraphicsExtensions{.pdf,.jpg,.png,.eps} %OK
% pour gérer le placement des figures (et tableau ...)

% pour modifier les marges facilement
\RequirePackage{geometry}
\RequirePackage{chngpage} %OK
% pour personnaliser les entêtes et pieds de page
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
%Pour les figure
% \RequirePackage{tikz}
% \usetikzlibrary{patterns,arrows,shapes}
%pour les maths
%\RequirePackage{latexsym}
% \RequirePackage{stmaryrd}	% commoment \varotimes ; arrondi \llceil \llfloor
%\RequirePackage{tensor}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
%\RequirePackage{wasysym}
%\RequirePackage{esint}
%\RequirePackage{subfig,wrapfig}
%\RequirePackage{floatflt}
%pour la mise en page
\RequirePackage{multicol}	% Utilisation à éliminer
\RequirePackage{tabularx}	% tableaux définis en proportion
\RequirePackage{booktabs}	% tableaux plus propres 
\RequirePackage{float}		% pour avoir la balise [H]
%\RequirePackage{supertabular}	% Permet d'avoir un tableau sur plusieures pages
\RequirePackage{multirow}	% fusionner des llignes dans un tableau
\RequirePackage{framed} 	% pour les listings
\RequirePackage[skins,many]{tcolorbox}	%Menu et encadrés (vérifier si skins et many sont utilises


\RequirePackage[frenchmath]{newtxmath}	% Polices pour les maths
% \setmainfont[]{Linux Libertine O}	% police serif normale
% \newfontfamily{\FA}{FontAwesome} 	% pour les TP

\RequirePackage{calc}	% pour définir des dimensions comme un rapport
\RequirePackage{ifthen}	% Structures conditionnelles

% \RequirePackage{qrcode}	% n'est plus utilisé depuis branly
% Type d'encryptage du document (pour l'entrée du texte)
% Selection de la langue du document
%\RequirePackage[frenchb]{babel} \selectlanguage{french}

\RequirePackage{titlesec}


\date{}

\geometry{%
	verbose,
	tmargin=1.5cm,
	bmargin=1cm,
	lmargin=1.5cm,
	rmargin=1.5cm,
	headheight=0.5cm,
	headsep=0.5cm,
	footskip=0.5cm,
	columnsep=1cm
	}

%\pagestyle{fancy}
%\fancyhead{} % clear all header fields
%\fancyhead[L]{\utilisecommande{type}{fancyfoot}{\@type}
  %\utilisecommande{classe}{fancyfoot}{~- \@classe} 
  %\utilisecommande{chapitre}{fancyfoot}{~- \@chapitre}}
%\fancyhead[R]{\thepage/\pageref{LastPage}}
%\fancyfoot{} % clear all footer fields
%\renewcommand{\headrulewidth}{0.4pt}

%% Pour les figure avec Inkscape
\RequirePackage{import}
\RequirePackage{xifthen}
\RequirePackage{pdfpages}
\RequirePackage{transparent}
\RequirePackage{xcolor}
%\pdfsuppresswarningpagegroup=1

\AtBeginDocument{
% format des listes
\renewcommand{\labelitemi}{\textbullet}
\renewcommand{\labelitemii}{$\cdot$}
% Permet d'avoir des captions en dehors des flottants.
\makeatletter
\def\@captype{figure}
\makeatother
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Commandes persos
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Pour ne pas frustrer pandoc

\newcommand*{\image}{\@ifstar\imagecaption\imageseule}

%\imagecaption[taille]{image}{legende}
\newcommand*{\imagecaption}[4][]{
	\begin{center}
		\includegraphics[width=#1\linewidth]{#2}
		
		\caption{#3}\label{#4}
	\end{center}}

%\image*[taille]{image}
\newcommand*{\imageseule}[2][]{
	%\begin{center}
		\includegraphics[width=#1\linewidth]{#2}
	%\end{center}
	}
	
	
\newcommand{\imageT}[1]{
	\begin{center}
		\includegraphics{#1}
	\end{center}
}

\newdimen\oldparindent  
\oldparindent=\parindent{}

\newcounter{Cmanip}
\setcounter{Cmanip}{0}

\newenvironment{manip}
{\begin{flushleft}\begin{minipage}{.99\textwidth}
\def\ancienBullet{\labelitemi}
\renewcommand{\labelitemi}{\FA \symbol{"F256}}
\refstepcounter{Cmanip}
{\sf\textbf{Manipulation \theCmanip}} 
\begin{itemize}}
{\end{itemize}\end{minipage}\end{flushleft} \renewcommand{\labelitemi}{\ancienBullet}}



%\newcommand{\norm}[1]{{\left \| #1 \right \|}}
\newcommand{\norm}[1]{\| #1 \|}
\newcommand{\ovr}[1]{\overrightarrow{#1}}
% \newcommand{\tl}[3]{\ensuremath{\tensor[_{#1}]{\left \{ \begin{array}{c}#2\\#3\end{array}	\right \}}{}}}
%\newcommand{\tc}[3][]{\ensuremath{\!\tensor[_{#2}]{\left \{ \begin{array}{c c} #3 \end{array}	\right \}}{_{#1}}}}

\newcommand*{\indiceGauche}[2]	{\ensuremath{{\vphantom{#2}}_{#1}{#2}}}

\newcommand{\torseur}[3][]{\indiceGauche{#2}{
	\left \lbrace
	\begin{array}{c c}
		#3
	\end{array}
	\right \rbrace}_{#1}}

\newcommand{\tc}[3][]{\torseur[#1]{#2}{#2}}

\newcommand{\bu}[1]{\textbf{\underline{#1}}}
\newcommand{\bb}[1]{\textbf{{#1}}}





\newcounter{Cquestion}
\setcounter{Cquestion}{0}
\newcommand{\question}[1]{
\begin{flushleft}
	\refstepcounter{Cquestion}
	{\bf (Question  \theCquestion)} \  #1
\end{flushleft}
}

% \newcommand{\question}[2]{
% \medskip
% \begin{itemize}
% 	\addtocounter{ques}{1}
% 	\item[\textbf{(Question  \theques)}] #1
% \end{itemize}
% \medskip
% }


\newcommand{\titrebox}[1]{
\begin{tcolorbox}
\huge \center{\sffamily \bfseries \Large #1}
\end{tcolorbox}
\setcounter{Cquestion}{0}
}

\newcommand{\mktitre}[1]{\titrebox{#1}}

\newtcolorbox{rem}[1]{
   freelance,
   breakable,
   title=\textbf{#1},
   left=0pt,
   right=0pt,
   width=.9\lw,
   enlarge left by=.05\lw,
   coltitle=black,
   frame code={},
   interior titled code={
     \draw[ultra thick] 
       ([yshift=5pt]frame.south west) |- ([xshift=160pt,yshift=-2pt]title.south west);
   }
}

\newtcolorbox{asavoir}[2][]{
	after title={\hfill\colorbox{white}{\color{black}{À Savoir}}},
	fonttitle=\bfseries,
	title={#2},
	colback=white,
	sharp corners=downhill,
	#1
}

\newtcolorbox{obj}[1][Objectif]{
	fonttitle=\bfseries,
	title={#1},
	colback=white
}

% Raccourci à ne plus utiliser
\newcommand{\lw}{\linewidth}

% forme différentielle
\newcommand{\ddt}[1]{\frac{\text{d}\,#1}V{\text{d\,}t}}
\newcommand{\ddv}[2][0]{\left [ \ddt{#2} \right]_{R_{#1}}}

% vecteur colonne
\newcommand{\vc}[1]{\left (
\begin{array}{c}
		#1
\end{array}
\right )}

% opérateur d'inertie
\newcommand{\I}{\ensuremath{\widetilde{\text{I}}}}

\titleformat{\section}
	{\Large\raggedright\sf\bfseries}
	{\thesection}{1em}
	{}
	[\titlerule]
	
\titleformat{\subsection}
	{\large\raggedright\sf\bfseries}
	{\thesubsection}{1em}
	{}
	[]		

%\renewcommand{\section}[1]{\begin{tcolorbox}\setcounter{ques}{0} \sectionORIG{#1}\end{tcolorbox}}
%\renewcommand{\section}{\setcounter{ques}{0} \sectionORIG}

\let\subsubsectionORIG\subsubsection
%\renewcommand{\subsubsection}[1]{\subsubsectionORIG*{\underline{#1}}}
\renewcommand{\subsubsection}[1]{\subsubsectionORIG*{#1}}

\newcommand{\dd}{\,\text{d}}
\newcommand{\euro}{\,€}


\newcommand{\V}[1]{\overrightarrow{V_{#1}}}
\newcommand{\Mam}[1]{\overrightarrow{M_{#1}}}
\newcommand{\acc}[1]{\overrightarrow{a_{#1}}}
\newcommand{\Mdyn}[1]{\overrightarrow{\delta_{#1}}}
\newcommand{\Mcin}[1]{\overrightarrow{\sigma_{#1}}}
\newcommand{\sol}[1]{\textbf{\underline{$#1$}}}

\DeclareMathOperator*{\tcoh}{T_{coh}}

%\newcommand{\qrda}{\image[.2]{/home/ben/texmf/2A.png}}
%\newcommand{\qrindex}{\image[.2]{/home/ben/texmf/index.png}}

\newcommand{\operateur}[1]{\begin{bmatrix} #1 \end{bmatrix}}

\newcommand{\base}[3]{\vec x_{#1}, \vec y_{#2},\vec z_{#3}}

\newcommand{\tv}[1]{\left \lbrace \mathcal{V}(#1)  \right \rbrace}
\newcommand{\tam}[1]{\left \lbrace \mathcal{T}_{#1}  \right \rbrace }
\newcommand{\tdyn}[1]{\left \lbrace \mathcal{D}(#1)  \right \rbrace }
\newcommand{\tcin}[1]{\left \lbrace \mathcal{C}(#1)  \right \rbrace }
\newcommand{\dtam}[1]{\left \lbrace \text{d}\mathcal{T}_{#1}  \right \rbrace }

\newcommand{\concours}[1]{\hfill \fbox{\scriptsize{#1}}}

% Représentation des nombres complexes
\newcommand{\comp}[1]{\underline{#1}}

% vim: ft=tex


